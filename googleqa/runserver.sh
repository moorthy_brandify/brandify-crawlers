#!/bin/bash

PROJECTDIR=$(dirname "$0");
cd $PROJECTDIR;
phantomjs --ssl-protocol=tlsv1 --ignore-ssl-errors=true --load-images=no --web-security=false ./appserver.js --debug=yes 
