#!/bin/bash

searchText=$1;
phantomjs --ssl-protocol=tlsv1 --ignore-ssl-errors=true --load-images=no --web-security=false phantomjsGoogleQA.js "$searchText"
