var webpage = require("webpage"),
    fs = require("fs");
var args = require('system').args;

var debug = false,
    searchUrl = "https://www.google.com/?hl=en",
    searchTerm = null;


var waitAndRespond = function () {
	var page     = crawlRequest.pageObject;
	var crawlingInProgress = page.evaluate(function() {
		try {
			return crawlingInProgress;
		} catch (ex) {
			return 1;
		}
	});

	if (crawlingInProgress == 0) {
		var reviewData = page.evaluate(function() {
			return reviewData;
		});
		var crawlerMessage = page.evaluate(function() {
			return crawlerMessage;
		});
		page.close();
		responseInfo = {
				code : 0,
				status : "ok",
		};
		if (reviewData.qaList) {
			responseInfo.qacount = reviewData.qaList.length;
		}
		responseInfo.message =  crawlerMessage;
		responseInfo.data  = reviewData;
		console.log(JSON.stringify(responseInfo));
		phantom.exit(0);
	} else {
		console.error("Crawling in progress");
		setTimeout(waitAndRespond,5000);
	}
}

var parseRequest = function (requestUrl) {
	requestUrl = decodeURI(requestUrl);
	requestInfo = requestUrl.split('?');
	requestInfo[0] = requestInfo[0].replace(/\W/g,'');
	if (requestInfo.length < 2) {
		return requestInfo;
	}

	var query = {};
	var pairs = requestInfo[1].split('&');
	for (var i = 0; i < pairs.length; i++) {
		var pair = pairs[i].split('=');
		query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
	}
    	requestInfo[1] = query

	return requestInfo;
}

var createPage = function () {

    var page = webpage.create();

    //set some headers to get the content we want
    page.customHeaders = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:22.0) Gecko/20130404 Firefox/22.0",
        "Accept-Language": "en"
    };

    //smaller size might get you the mobile versions of a site
    page.viewportSize = { width: 1280, height: 800 };

    //good to debug and abort request, we do not wish to invoke cause they slow things down (e.g. tons of plugins)
    page.onResourceRequested = function (requestData, networkRequest) {
        log(["onResourceRequested", JSON.stringify(networkRequest), JSON.stringify(requestData)]);
        //in case we do not want to invoke the request
        //networkRequest.abort();
    };

    //what dd we get
    page.onResourceReceived = function (response) {
        log(["onResourceReceived", JSON.stringify(response)]);
    };

    //what went wrong
    page.onResourceError = function (error) {
        log(["onResourceError", JSON.stringify(error)]);
    };

    page.onLoadStarted = function() {
        log(["loading page..."]);
    };

    page.onLoadFinished = function(status) {
        var currentUrl = page.evaluate(function() {
            return window.location.href;
        });
        log(["onLoadFinished", currentUrl, status]);
    };

    page.onConsoleMessage = function(msg) {
   	notifyConsole(msg);
    };
    page.onError = function(msg, trace) {
	var msgStack = ['ERROR: ' + msg];
	if (trace && trace.length) {
		msgStack.push('TRACE:');
		trace.forEach(function(t) {
		msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function + '")' : ''));
	});
	}
	notifyConsole(msgStack.join('\n'));
    };

    return page;
}

var startGoogleSearch = function (crawlRequest) {
	var page = crawlRequest.pageObject;
	notifyConsole("INFO: Going to search [" + crawlRequest.search + "]!!");
	page.open(searchUrl, function () {
		setTimeout(function () {
			//for debugging purposes
			page.render("./snapshots/googlesearch-initpage.png");
			var injected = page.injectJs('jquery.min.js');
			if (!injected) {
				throw Error("ERROR: JQuery could not be injected");
			} else {
				console.error(["INFO: JQuery injected successfully"]);
			}
			var f = page.evaluate(function (searchTerm) {
				$("input").val(searchTerm);
				$("form").submit();
			}, crawlRequest.search);
			setTimeout(function () {
				googleSearchResults(crawlRequest);
			}, 3500);
		}, 2000);
	});
};

var checkQuestionAnswerPopupAndCrawl = function (crawlRequest) {
	var page = crawlRequest.pageObject;
	log("Popup has appeared ?");
	page.render("./snapshots/questionpopup-init.png");
	var status = page.evaluate(function() {
		return checkPopup();
	});
	if (!status) {
		notifyConsole("ERROR: No popup appeared on clicking question answer. Exitting!!");
		phantom.exit(1);
	}

	setTimeout(function() {
		scrapeQuestionAnswers(crawlRequest);
	}, 3000);
};

var scrapeQuestionAnswers = function (crawlRequest) {
	var page = crawlRequest.pageObject;
	var questionCount = page.evaluate(function() {
		return numQuestions;
	});

	if (questionCount > 0) {
		page.evaluate(function() {
			loadAllQuestions();
		});
	}
};

var googleSearchResults = function (crawlRequest) {
	//for debugging purposes
	var page = crawlRequest.pageObject;
	page.render("./snapshots/googlesearch-resultpage.png");
	var injected = page.injectJs('jquery.min.js');
	if (!injected) {
		throw Error("JQuery could not be injected");
	} else {
		log("JQuery injected successfully");
	}
	injected = page.injectJs('crawlfunctions.js');
	if (!injected) {
		throw Error("Crawler functions could not be injected");
	} else {
		log("Crawler functions injected successfully");
	}

	// Check if Question link exists. If yes, then click the element
	var status = page.evaluate(function() {
		initGoogleSearchResults();

		var rhs = $('#rhs');
		console.error("Clicked the question href");
		//var el = document.querySelector('a span._qsu'); // CHANGE
		var el = document.querySelector('a span.QlPmEd');
		if (el == null) {
			console.error("Not able to find 'See all questions' link");
			crawlingInProgress = 0;
			return 0;
		}
		try {
			locationKPName = document.querySelector('div[data-local-attribute="d3bn"] span').innerHTML;
			print
		}  catch (e) {};
		var ev = document.createEvent("MouseEvent");
		ev.initEvent("click", true, true);
		el.dispatchEvent(ev);
		return 1;
	});
	if (!status) {
		notifyConsole("ERROR: Unable to see or click on questions link. Exitting!!");
		return;
	}

	// Wait for 2 seconds for the question popup to appear.
	setTimeout(checkQuestionAnswerPopupAndCrawl, 4000, crawlRequest);

	return;
};

var log = function (args) {
    if (debug) {
        console.error(args);
    }
}

var notifyConsole = function (args) {
        console.error(args);
}

console.error = function () {
    require("system").stderr.write(Array.prototype.join.call(arguments, ' ') + '\n');
};

var page = createPage();

if (!args[1] || !args[1].length) {
	notifyConsole("ERROR: No search argument provided");
	phantom.exit(1);
}
crawlRequest = {};
crawlRequest.pageObject = page;
crawlRequest.search = args[1];
startGoogleSearch(crawlRequest);
waitAndRespond();
