var webserver = require('webserver');
var system    = require('child_process');
var httpPort  = 7080;
var debug     = true;
var MAXRETRIES= 3;

var createServer = function () {
	server = webserver.create();
	console.log("INFO: Server Created and runs in port [" + httpPort + "]");
	var listening = server.listen(httpPort, function (request, response) {
		requestInfo = parseRequest(request.url);
		response.headers = {"Cache": "no-cache", "Content-Type": "application/json"};
		switch (requestInfo[0]) {
			case 'crawl':
				console.log("INFO: Going to search [" + requestInfo[1].search + "]");
				crawlRequest = {};
				crawlRequest = requestInfo[1];
				crawlRequest.responseObject = response;
				crawlRequest.currentTryCount= 0;
				runCrawler(crawlRequest);
				break;
			default:
				responseInfo = {
					code : 99,
					status : "notok",
					message : "invalid action [" + requestInfo[0] + "]"
				};
				response.statusCode = 404;
				response.write(JSON.stringify(responseInfo));
				response.close();
		}
	});
	if (!listening) {
		console.log("ERROR: Could not create web server listening on port " + port);
		phantom.exit(1);
	}
}

var runCrawler = function (crawlRequest) {

	if (crawlRequest.retrycount && parseInt(crawlRequest.retrycount)) {
		crawlRequest.retrycount = parseInt(crawlRequest.retrycount);
		if (crawlRequest.retrycount > MAXRETRIES) {
			crawlRequest.retrycount = MAXRETRIES;
		}
	} else {
		crawlRequest.retrycount = 1;
	}

	system.execFile ("./runcrawler.sh", [crawlRequest.search], null, function (err, stdout, stderr) {
		console.log("========STDOOUT=========");
		//console.log(stdout);
		console.log("JSON output is suppressed...");
		console.log("<=======================");
		console.log("========STDERR==========");
		console.log(stderr);
		console.log("<=======================");
		waitAndRespond(crawlRequest, stdout);
	});
}

var waitAndRespond = function (crawlRequest, crawlContent) {
	crawlRequest.currentTryCount++;
	var response = crawlRequest.responseObject;
	responseInfo = {};
	try {
		responseInfo = JSON.parse(crawlContent);
	} catch (ex) {
		console.log("ERROR: Some Exception in parsing the response [" + ex + "]");
		responseInfo = {
			code : 99,
			status : "not ok"
		};
	}
	if (!responseInfo.qacount && crawlRequest.currentTryCount < crawlRequest.retrycount) {
		console.log("Retrying again to search [" + crawlRequest.search + "]");
		runCrawler(crawlRequest);
		return;
	}
	response.statusCode = 200;
	response.write(JSON.stringify(responseInfo));
	response.close();
	return;
}

var parseRequest = function (requestUrl) {
	requestUrl = decodeURI(requestUrl);
	requestInfo = requestUrl.split('?');
	requestInfo[0] = requestInfo[0].replace(/\W/g,'');
	if (requestInfo.length < 2) {
		return requestInfo;
	}

	var query = {};
	var pairs = requestInfo[1].split('&');
	for (var i = 0; i < pairs.length; i++) {
		var pair = pairs[i].split('=');
		query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
	}
	requestInfo[1] = query
	console.log ("INFO: Request " + JSON.stringify(requestInfo));
	return requestInfo;
}

createServer();
