popupWin = null;
popupScrollDiv = null
numQuestions = 0;
reviewData = {};
reviewList = new Array();
dataFID = null;
crawlerMessage = "";
locationKPName = null;

crawlingInProgress = 1;

function initGoogleSearchResults() {
	//dataFIDElement = document.querySelector("a[data-reply_source='local-search-reviews-list']");
	dataFIDElement = document.querySelectorAll("a[data-fid]")[0];
	if (!dataFIDElement) {
		crawlerMessage = "No Knowlege panel found";
	} else {
		el = dataFIDElement.attributes['data-fid'];
		if (el) {
			dataFID = el.value;
		} else {
			crawlerMessage = "No Knowlege panel found";
		}
	}
	return;
}

function checkPopup() {    
	//popupWin = document.querySelector('div div._fPu'); //CHANGE
	popupWin = document.querySelector('div div.kfoSBc');
	if (popupWin == null) {
		console.log("Question & Answer section not popped up ...");
		return 0;
	} else {
		console.log("Question & Answer section popped up successfully ...");
	}

	//el = popupWin.querySelector('div._fPu div._M6u'); //CHANGE
	el = popupWin.querySelector('div.kfoSBc div.y4DTBc');
	if (el) {
		reviewData.locationName = el.innerHTML;
	}

	el = popupWin.querySelector('div.kfoSBc div.Xv46H');
	if (el) {
		reviewData.locationAddress = el.innerHTML;
	}

	popupScrollDiv = $(popupWin).children()[1];
	scrollDivDown(popupScrollDiv, 1000, 5, 0);
	return 1;
}

function scrollDivDown(divelem, heightpos, maxcount, curcount) {

        if (curcount < maxcount) {
                setTimeout(function(el, hpos, mxc, crc) {
                        el.scrollTop = el.scrollHeight + hpos;
                        crc = crc+1;
                        scrollDivDown(el, hpos, mxc, crc);
                }, 500, divelem, heightpos, maxcount, curcount);
        } else {
		//questionDivs = document.querySelectorAll('div div._fPu div._YOu'); //CHANGE
		questionDivs = document.querySelectorAll('div div.kfoSBc div.rbheYb');
		numQuestions = questionDivs.length;
		console.log("numer of questions is " + numQuestions);
		for (i = 0; i < numQuestions; i++) {
			qDiv = questionDivs[i];
			//qDivAhref = qDiv.querySelector("div._LKu div._GMs"); //CHANGE
			qDivAhref = qDiv.querySelector("div.LR1OTe div.t6X3Re a");
			reviewList[i] = {};
			reviewList[i].questionText = qDiv.querySelector('div.mq1Pic').innerHTML;
			if (qDivAhref && qDivAhref.attributes['data-annotation-id']) {
				reviewList[i].questionId = qDivAhref.attributes['data-annotation-id'].value
			}
		}
	}
}

function loadAllQuestions() {

	crawlingInProgress = 1;
	// Start with 1st Question...
	extractQAAnswers(0);
}

function finishCrawling() {
	crawlingInProgress = 0;
	reviewData.qaList = reviewList;
	reviewData.locationKPName = locationKPName;
	//console.log(JSON.stringify(reviewList));
	console.log("Finished crawling " + numQuestions + " questions");
}

function extractQAAnswers(reviewIdx) {

	// Finish extracting al QA for this site if reviewIdx is maxed out
	if (reviewIdx >= numQuestions || !reviewList[reviewIdx] || !dataFID) {
		finishCrawling();
		return;
	}
	var questionId = reviewList[reviewIdx].questionId;
	var fetchUrl = "//www.google.com/async/pqaA?hl=en&yv=2&async=fid:" + dataFID + ",id:GReview,q:,annotid:" + questionId + ",_id:pqaA,_id:pqaA,_pms:s,_fmt:pc";
	$.ajax({
		url : fetchUrl,
		reviewIdx : reviewIdx,
	})
	.done(function( respdata ) {
		try {
			var response ;
			if (typeof(respdata) == 'object') {
				response = respdata;
			} else if (typeof(respdata) == 'string') {
				response = respdata;
			} else {
				response = JSON.parse(respdata);
			}

			if (typeof(respdata) == 'string') {
				htmlContent = response;
			} else {
				htmlContent = response[1][1];
			}
			htmlContent = htmlContent.replace(/\n/gi, ' ');
			htmlContent = htmlContent.replace(/^.*?<div/gi, '<div');
			htmlContent = htmlContent.replace(/<script.*$/gi, '');
			parseQA(this.reviewIdx, htmlContent);
		} catch (err) {
			console.log("Error while crawling and parsing " + this.url);
			console.log(err);
			extractQAAnswers(this.reviewIdx+1);
		}
	})
	.fail(function( respdata ) {
		console.log("Error while crawling and parsing " + this.url);
		console.log(respdata);
		extractQAAnswers(this.reviewIdx+1);
	});;
}

function parseQA(reviewIdx, htmlContent) {
	var qaContent = document.createElement("div");
	qaContent.innerHTML = htmlContent;

	//allQAPortitions = qaContent.querySelectorAll('div._LKu');
	allQAPortitions = qaContent.querySelectorAll('div.LR1OTe');
	//reviewList[reviewIdx].postedBy = allQAPortitions[0].querySelector('div._exw').innerHTML;//CHANGE
	reviewList[reviewIdx].postedBy = allQAPortitions[0].querySelector('div.CfHx4b').innerHTML;
	//reviewList[reviewIdx].questionText = allQAPortitions[0].querySelector('div._Isv').innerHTML;
	reviewList[reviewIdx].questionText = allQAPortitions[0].querySelector('div.EDNFOd').innerHTML; //CHANGE
	el = allQAPortitions[0].querySelector('div.yEKpFf span.C9Sc7e');
	if (el) {
		reviewList[reviewIdx].questionUpVotes = parseInt(el.innerHTML);
	}

	reviewList[reviewIdx].answers = new Array();
	for (i = 2; i < allQAPortitions.length; i++) {
		answerItem = {};
		if (allQAPortitions[i].querySelector('div.LR1OTe div.CfHx4b')) {
			answerItem.authorName = allQAPortitions[i].querySelector('div.LR1OTe div.CfHx4b').innerHTML;
			el = allQAPortitions[i].querySelector('div.LR1OTe span.ESb0uf');
			if (el) {
				answerItem.authorType = el.innerHTML;
			}
			el = allQAPortitions[i].querySelector('div.LR1OTe div.uVF3Ge');
			if (el) {
				content = el.innerHTML;
				matched = content.match(/.*?(\d+)\s+review.*/i);
				if (matched && matched[1]) {
					answerItem.authorReviews = parseInt(matched[1]);
				}
				matched = content.match(/.*?(\d+)\s+photo.*/i);
				if (matched && matched[1]) {
					answerItem.authorPhotos = parseInt(matched[1]);
				}
			}
			el = allQAPortitions[i].querySelector('div.LR1OTe div.eIlJMe');
			if (el) {
				answerItem.whenPosted = el.innerHTML;
			}
			el = allQAPortitions[i].querySelector('div.LR1OTe div.CKgeL');
			if (el) {
				answerItem.answerText = el.innerHTML;
			}
			el = allQAPortitions[i].querySelector('div.yEKpFf span.C9Sc7e');
			if (el) {
				answerItem.answerUpVotes = parseInt(el.innerHTML);
			}
			el = allQAPortitions[i].querySelector('div.CvMwY div.DAuLOd a');
			if (el) {
				answerItem.authorAvatarUrl = el.attributes['href'].value;
			}
			el = allQAPortitions[i].querySelector('div.CvMwY div.DAuLOd a div.MPpN7');
			if (el) {
				if (el.attributes['style']) {
					authorPhoto = el.attributes['style'].value.replace(/^.*\/\//g, 'https://').replace(/\.jpg.*$/g, '.jpg');
					answerItem.authorPhoto = authorPhoto;
					authorLevel = authorPhoto.match(/.*w40-h40-ba(\d+).*/);
					if (authorLevel && authorLevel[1]) {
						answerItem.authorLevel = parseInt(authorLevel[1]) + 2;
					} else {
						answerItem.authorLevel = 3;
					} 
				}
			}
			reviewList[reviewIdx].answers.push (answerItem);
		}
	}
	qaContent.innerHTML = '';
	crawlerMessage = "Crawling done";
	extractQAAnswers(reviewIdx+1);
}

